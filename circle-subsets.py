from math import radians, cos, sin, asin, sqrt

#haversine function from http://code.activestate.com/recipes/576779-calculating-distance-between-two-geographic-points/
AVG_EARTH_RADIUS = 6371  # in km

def haversine(lat1,lng1,lat2,lng2):
    """ Calculate the great-circle distance bewteen two points on the Earth surface.

    :input: two 2-tuples, containing the latitude and longitude of each point
    in decimal degrees.

    Example: haversine((45.7597, 4.8422), (48.8567, 2.3508))

    :output: Returns the distance bewteen the two points.
    The default unit is kilometers. Miles can be returned
    if the ``miles`` parameter is set to True.

    """

    # convert all latitudes/longitudes from decimal degrees to radians
    lat1, lng1, lat2, lng2 = map(radians, (lat1, lng1, lat2, lng2))

    # calculate haversine
    lat = lat2 - lat1
    lng = lng2 - lng1
    d = sin(lat * 0.5) ** 2 + cos(lat1) * cos(lat2) * sin(lng * 0.5) ** 2
    h = 2 * AVG_EARTH_RADIUS * asin(sqrt(d))
    return h  # in kilometers

#the coordinates of the centers of the 30 biggest cities in the Netherlands. source:http://www.tageo.com/index-e-nl-cities-NL.htm
city_centers=[[52.370,4.890],[51.930,4.480],[52.080,4.280],[52.100,5.110],[51.440,5.470],[51.570,5.070],[53.230,6.570],[52.360,5.170],[51.580,4.770],[51.840,5.850],[52.220,5.960],[52.220,6.890],[52.390,4.620],[51.990,5.910],[52.450,4.820],[52.160,5.380],[51.700,5.310],[52.300,4.700],[50.850,5.690],[51.800,4.670],[52.170,4.490],[52.070,4.490],[52.520,6.090],[52.790,6.900],[52.040,5.650],[51.010,5.860],[52.010,4.360],[50.910,6.030],[52.640,4.750],[53.210,5.790]]

#read in datafile
data_s=open('/home/geophy/Desktop/buildings-year-known.csv','r')
data_slines=data_s.readlines()
data_list=[]
for i in range(1,len(data_slines)):
    data_slines[i]=data_slines[i].replace('\n','')
    data_list.append(data_slines[i].split(','))

#find buildings in a ring around the city located 10km from the center with a 1km width
tenkm=[]
for i in range(len(city_centers)): 
    for j in range(len(data_list)):
        if 10<=haversine(city_centers[i][0],city_centers[i][1],float(data_list[j][1]),float(data_list[j][2]))<11:
            if data_list[j] in tenkm:
                continue
            else:
                tenkm.append(data_list[j])

#write into file
g=open('/home/geophy/Desktop/radius-subset.csv','w')
for k in range(len(tenkm)):
    g.write('%s,%s,%s,%s\n'%(tenkm[k][0],tenkm[k][1],tenkm[k][2],tenkm[k][3]))
g.close()