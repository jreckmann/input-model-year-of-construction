#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 23 09:17:31 2016

@author: geophy
"""

#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 17 11:24:40 2016

@author: geophy
"""
import numpy as np
import random
import subprocess
from sklearn.neighbors import DistanceMetric
import matplotlib.pyplot as plt
import math

#function to check if three points lie approximately on a line or span a right angle triangle
def triangles(distance1,distance2,distance3):
    a=sorted([distance1,distance2,distance3])[2]
    b=sorted([distance1,distance2,distance3])[1]
    c=sorted([distance1,distance2,distance3])[0]
    if a>0.999*(b+c):
        geom=True
    #comment the following out for lines only
    elif 0.999*(b**2+c**2)<a**2<1.001*(b**2+c**2):
        geom=True
    ############
    else:
        geom=False
    return geom
#    return True
metric=DistanceMetric.get_metric('haversine')
neighborpoints=5#number of neighbors that should be checked for their geometrical location around the testpoint

file_input=raw_input('insert path to the datafile: ')
file_output=raw_input('insert path to outputfile: ')
file_image=raw_input('insert path to the image: ')
image_title=raw_input('insert image title: ')
hist_output=raw_input('insert path to the histogram: ')
bracket=int(raw_input('insert the acceptable error of years: '))
N=int(raw_input('insert number of testpoints: '))
runs=int(raw_input('insert number of runs: '))
bins=int(raw_input('insert the accuracy in years: '))

#for m in range(1,neighborcount+1):#a run for every neighbor up to the inserted amount
rights=[]#create an empty list to store the accuracy of the runs
unreliable=[]
deltayears=[]
for q in range(runs):
    full_data_list=np.genfromtxt('%s'%file_input,delimiter=',') #read data into numpy array
    rand=random.sample(range(len(full_data_list)),N)#get a list of random integers in the range of the data amount
    testpoints=np.zeros([N,4])#create an empty array
     
    for i in range(N):#fill the empty array with testpoints chosen with the list of random integers
        testpoints[i]=full_data_list[rand[i]]

    data_list=np.delete(full_data_list,[rand],0)#delete the testpoints from the data
    
    from sklearn.neighbors import NearestNeighbors#use the NeirestNeighbor algorithm from scikit learn to structure the data for further use
    neigh = NearestNeighbors(n_neighbors=neighborpoints,metric='haversine')
    neigh.fit(np.radians(data_list[:,[1,2]]))
    
    year=[]
    for j in range(N):#for each of testpoint:
        years=0
        distances,neighb=neigh.kneighbors(np.radians(testpoints[[j],[[1,2]]]))#find the neighbors of a testpoint
        for n in range(neighborpoints-1):#find the nearest pair of neighbors that is in a straigh line with the testpoint or spans a right angle triangle.
            for z in range(n+1,neighborpoints):
                if triangles(distances[0][n],distances[0][z],metric.pairwise([np.radians(data_list[[neighb[0][n]],[1,2]]),np.radians(data_list[[neighb[0][z]],[1,2]])])[0][1])==True:
                    if data_list[neighb[0][n]][3]-bracket<=data_list[neighb[0][z]][3]<=data_list[neighb[0][n]][3]+bracket:#check if the years of construction of the neighbors is in the specified range
                        years=((float(data_list[neighb[0][n]][3]))+float(data_list[neighb[0][z]][3]))/float(2)
                    else:
                        years=0
                if years!=0:
                    break
            if years!=0:
                break
        year.append(years)
    print(q*float(100)/float(runs))
    right=0
    unable=0
    for l in range(N):
        if year[l]!=0:
            deltayears.append(math.fabs(float(year[l])-float(testpoints[[l],[3]])))
        #check if the average year of construction around a point is within +-5 years of the year of construction of the testpoint
        if year[l]-bins<=testpoints[[l],[3]]<=year[l]+bins:
            right+=1
        if year[l]==0:
            unable+=1
    rights.append((float(right)/float(N-unable)))
    unreliable.append(float(unable)/float(N))
#make a histogram of the accuracy
plt.hist(deltayears,50,[1,300])
plt.xlabel('|real year - year neighbors|')
plt.ylabel('count')
plt.title('%s'%image_title)
plt.savefig('%s.png'%hist_output,dpi=200)
plt.clf()
 #calculate the quartiles for all runs having the same amount of neighbors
med=(np.percentile(rights,50))
lq=(np.percentile(rights,25))
uq=(np.percentile(rights,75))
uw=(np.percentile(rights,100))
lw=(np.percentile(rights,0))
unreliables=(sum(unreliable)/float(runs))

g=open('%s'%(file_output),'w')#write the quartiles into a file
g.write('%i %f %f %f %f %f %f\n'%(1,lw,lq,med,uq,uw,unreliables))
g.close()

proc=subprocess.Popen(['gnuplot','-p'],shell=True, stdin=subprocess.PIPE)#use gnuplot to make boxplots
proc.stdin.write('set boxwidth 0.2\n set style fill solid 0.1 border -1\n set xrange[0:2]\n set xtics 1\n set yrange [0:1.1]\n set ytics 0.05\n')
proc.stdin.write('set xlabel \'\' \n set ylabel \'Accuracy\'\n set title \'%s\'\n'%image_title)
proc.stdin.write('set terminal png size 1024,768\n')
proc.stdin.write('set output \'%s\'\n'%file_image)
proc.stdin.write('plot \'%s\' using 1:3:2:6:5 with candlesticks title \'average discarded predictions: %f\' lt -1 lw 2 whiskerbars, \'\' using 1:4:4:4:4 with candlesticks lt -1 lw 2 notitle\n'%(file_output,unreliables))
proc.stdin.write('quit\n')

