from keras.models import Sequential
from keras.layers import Dense
import numpy as np
import math
from datetime import datetime
from keras.optimizers import Adam
print(datetime.now().strftime('%H:%M:%S'))
from keras.layers import Dropout
from keras.constraints import maxnorm
from keras.regularizers import l2

np.random.seed(5)

#Function to find a matrix of neighboring points around a building
def find_grid(data_point,Data_Matrix,Cell_Number):
    neighbor_grid=[]
    for q in range(int(round((data_point[1]*10000)-507281)-Cell_Number),int(round((data_point[1]*10000)-507281)+Cell_Number+1)):
        for p in range(int((round(data_point[2]*10000)-32713.15)-Cell_Number),int((round(data_point[2]*10000)-32713.15)+Cell_Number+1)):
            if q==int(round((data_point[1]*10000)-507281)) and p==int((round(data_point[2]*10000)-32713.15)):
                continue
            else:
                neighbor_grid.append(Data_Matrix[q,p])
    return neighbor_grid
    
CellNumber=10

#make a sparse matrix with the appropriate number of cells
from scipy import sparse
data_matrix=sparse.dok_matrix((33688+(4*CellNumber),53953+(4*CellNumber)),dtype=int)

#read in the data
data_list=np.genfromtxt('/home/geophy/Desktop/buildings-year-known.csv',delimiter=',')

#rescale the data to remove the gap from 0 to the smallest year of construction in the data.
for e in range(len(data_list)):
    if data_list[e][3]-900<=0:
        np.delete(data_list,e,0)
    else:
        data_list[e][3]=(data_list[e][3]-900)

#fill the sparse matrix with the data
counter=[]
for l in range(len(data_list)):
    if data_matrix[round(data_list[l,1]*10000-507281)+CellNumber,round((data_list[l,2]*10000)-32713.15)+CellNumber]!=0:
        data_matrix[round(data_list[l,1]*10000-507281)+CellNumber,round((data_list[l,2]*10000)-32713.15)+CellNumber]=round((data_matrix[round(data_list[l,1]*10000-507281)+CellNumber,round((data_list[l,2]*10000)-32713.15)+CellNumber]+(data_list[l,3]))/2.0)
    data_matrix[round(data_list[l,1]*10000-507281)+CellNumber,round((data_list[l,2]*10000)-32713.15)+CellNumber]=(data_list[l,3])

#make set of random data to be used for training and prediction and remove buildings without neighbors in their respective grids from it.
counter=0      
grid_set=[]
data_set=[]
for rpoint in np.random.randint(0,len(data_list),70000):
    chi=find_grid(data_list[rpoint],data_matrix,CellNumber)
    if max(chi)!=0:
        grid_set.append(chi)
        data_set.append(data_list[rpoint,3])


#convert the year of constructions to a binary matrix
for q in range(len(data_set)):
    z=np.zeros(1120,int)
    z[int(data_set[q])]=1
    data_set[q]=z

#make a training subset of the random dataset for training and convert it to a numpy arrow of appropriate size
train_grid=[]
train_data=[]
for u in range(5000):
    train_grid.append(grid_set[u])
    train_data.append(data_set[u])

train_grid,train_data=np.array(train_grid).reshape(-1,440),np.reshape(np.array(train_data),(-1,1120))

#make a prediction subset of the random dataset for training and convert it to a numpy arrow of appropriate size  
eval_grid=[]
eval_data=[]
for z in range(len(grid_set)-10000,len(grid_set)):
    eval_grid.append(grid_set[z])
    eval_data.append(data_set[z])

eval_grid,eval_data=np.array(eval_grid).reshape(-1,440),np.reshape(np.array(eval_data),(-1,1120))

print(datetime.now().strftime('%H:%M:%S'))
print("nn starts now")

X=train_grid
Y=train_data

#########################################################################################
#Neural network part. Add layers,regularizers and other parameters here:
model=Sequential()
model.add(Dense(440,input_dim=440,init='uniform',activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(800,init='uniform',activation='relu',W_regularizer='l2'))
model.add(Dropout(0.2))
model.add(Dense(1120,init='uniform',activation='softmax'))

ad=Adam(lr=1)

model.compile(loss='categorical_crossentropy',optimizer=ad,metrics=['accuracy'])
########################################################################################

model.fit(X,Y,nb_epoch=100)

scores=model.evaluate(X,Y)
print(datetime.now().strftime('%H:%M:%S'))
print("Baseline Error: %.2f%%" % (100-scores[1]*100))

#predict on the prepared subset
predictions=model.predict(eval_grid)

#calculate the difference between the predicted and the real year of construction and print it
test_list=[]
for c in range(len(eval_grid)):
    for c2 in range(len(eval_data[c])):
        if eval_data[c][c2]!=0:
            test_list.append(c2)
            
diff=[]
pred=[]
for count in range(len(predictions)):
    pred.append(predictions[count].tolist().index(max(predictions[count]))+900)
    diff.append(math.fabs(predictions[count].tolist().index(max(predictions[count]))-test_list[count]))
 
print(datetime.now().strftime('%H:%M:%S'))
summe=sum(diff)/float(len(diff))
print('average difference: %f'%summe)


#below is the code to predict on the whole dataset and output the difference in predicted and real year of construction
'''
difference=[]
for kappa in range(len(data_list)):
    qu=np.array(find_grid(data_list[kappa],data_matrix,CellNumber))
    difference.append(math.fabs((np.argmax(model.predict(qu.reshape(1,1680))))-data_list[kappa,3]))
    if kappa%100==0:
        print(kappa)

            
print(datetime.now().strftime('%H:%M:%S'))
print(len(impossible))
print('average difference: %f'%(sum(difference)/len(difference)))
'''