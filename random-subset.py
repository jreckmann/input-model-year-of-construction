#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 16:01:42 2016

@author: geophy
"""
import numpy as np
import random

#make a set of random integers and use these to make a random subset of the data
data_list=np.genfromtxt('/home/geophy/Desktop/buildings-year-known.csv',delimiter=',') #read data into numpy array

index=random.sample(range(len(data_list)),10000)

f=open('/home/geophy/Desktop/random-sample.csv','w')

for i in index:
    f.write('%i,%f,%f,%i\n'%(int(data_list[i][0]),data_list[i][1],data_list[i][2],int(data_list[i][3])))
f.close()