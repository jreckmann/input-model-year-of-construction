#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 11 10:18:05 2016

@author: geophy

"""


#######
#######Too many variables, not working on it currently!######
#######


import numpy as np
from sklearn.cluster import DBSCAN
import matplotlib.pyplot as plt

def calculate_age(neighbors,average):#calculate the year of construction based on a weighted average of the years of constructions in the neighborhood
    counter=0
    year=0
    for i in range(len(neighbors)):
        if neighbors[i,3]==average:
            counter+=5
            year+=5*i
        elif 10-average<neighbors[i,3]<10+average:
            counter+=4
            year+=4*i
        elif 20-average<neighbors[i,3]<20+average:
            counter+=3
            year+=3*i
        elif 40-average<neighbors[i,3]<40+average:
            counter+=2
            year+=2*i
        elif 80-average<neighbors[i,3]<80+average:
            counter+=1
            year+=i
    return year/counter
    

def locate_average(intervals,center,point):#locate the average age of the neighborhood in the distance intervals
    distance=np.linalg.norm(point[1:3]-center[1:3])
    for i in range(len(intervals)):
        if distance<=intervals[i][1]:
            average=intervals[i][0]
            break
    return average

def find_neighbors(point,cluster,radius):#find the buildings in radius of the point
    neighbors=[]
    for i in range(len(cluster)):
        if np.linalg.norm(point[1:3]-cluster[[i],[1,2]])<radius:
            neighbors.append(cluster[i])
    return neighbors


def missing_points(data,labels):#find the data points with missing year of construction and delete them from the used data
    missing=[]
    labl=[]
    for i in range(len(data)):
        if np.isnan(data[[i],[3]]):
            missing.append(data[i])
            labl.append(labels[i])
            data=np.delete(data,i,0)
            labels=np.delete(labels,i,0)
    for j in range(len(missing)):
        missing[i].append(labl[i])
    return np.asarray(missing),data,labels



def gather_cluster(data,point,labels):#find the cluster that belongs to the data point in question
    a=point[4]
    cluster=[]
    for i in range(len(labels)):
        if labels[1]==a:
            cluster.append(data[i])
    return np.asarray(cluster)
            

def find_center(cluster,amount_center,radius_center):#reminder: find alternative to pop for numpy arrays and test how add works here
    years=[]
    sorted_cluster=cluster[cluster[:,3]]
    for i in range(amount_center):
        counter=0
        temp=sorted_cluster[i]
        sorted_cluster=np.delete(sorted_cluster,i,0)
        years.append(temp[[i],[3]])
        for j in range(len(cluster)):
            if np.linalg.norm(temp[[i],[1,2]]-cluster[[j],[1,2]])<radius_center:
                years[i]+=cluster[[j],[3]]
                counter+=1
        years[i]=years[i]/counter
        counter=0
        sorted_cluster.add(temp)
    return sorted_cluster[years.index(min(years))]

                          
def find_intervals(cluster,center,bin_width):#find the average year of construction in radius intervals from the center
    distances=[]
    age_bin=[]
    counter=0
    for i in range(len(cluster)):
        distances.append(np.linalg.norm(center[1:3]-cluster[[i],[1,2]]))
    interval=max(distances)/bin_width
    cluster_dist=np.append(cluster,distances,axis=1)
    bins=np.linspace(0,max(distances),interval)
    for j in range(len(bins)-1):
        counter=0
        age=0
        for k in range(len(cluster_dist)):
            if bins[i]<cluster_dist([[j],[4]])<=bins[i+1]:
                age+=cluster_dist([[j],[3]])
                counter+=1
        age/=counter
        age_bin.append([age,bins[i+1]])
    return age_bin



   
data_list=np.genfromtxt('/home/geophy/Desktop/constructiony/data subsets/buildings-subset-sparse.csv',delimiter=',') #read data into numpy array



db=DBSCAN(eps=0.003,min_samples=6).fit(data_list[:,[1,2]])#cluster the data with DBSCAN
core_samples_mask=np.zeros_like(db.labels_,dtype=bool)
core_samples_mask[db.core_sample_indices_]=True
labels=db.labels_


#---------------------------------------------------------------------------------------plot clusters, not relevant---------------------------
n_clusters_=len(set(labels))-(1 if -1 in labels else 0)

print('Estimated number of clusters: %d' % n_clusters_)

unique_labels=set(labels)
colors=plt.cm.Spectral(np.linspace(0,1,len(unique_labels)))


for k,col in zip(unique_labels,colors):
    if k==-1:
        continue
    
    class_member_mask=(labels==k)    
    xy=data_list[:,[1,2]][class_member_mask&core_samples_mask]
    plt.plot(xy[:,0],xy[:,1],'o',markerfacecolor=col,markeredgecolor='k',markersize=4)
    
    xy=data_list[:,[1,2]][class_member_mask&-core_samples_mask]
    plt.plot(xy[:,0],xy[:,1],'o',markerfacecolor=col,markeredgecolor='k',markersize=2)
    
plt.title('Estimated number of clusters: %d'%n_clusters_)
plt.show()
#-------------------------------------------------------------------------------------------------------------------------------------------
#select random data points for initial testing-------------------------------------
rand=np.random.random_integers(0,len(data_list)-1,100)
missing=[]
lab=[]
for l in rand:
    missing.append(data_list[l])
    lab.append(labels[l])
    data_list=np.delete(data_list,l,0)
    labels=np.delete(labels,l,0)
for k in range(len(missing)):
    missing[k].append(lab[k])


#missing,data_list,labels=missing_points(data_list)
yearsofconst=[]

for m in range(len(missing)):
    cluster=gather_cluster(data_list,data_list[m],labels)
    center=find_center(cluster,10,0,001)
    intervals=find_intervals(cluster,center,0,001)
    average=locate_average(intervals,center,missing[m])
    neighbors=find_neighbors(missing[m],cluster,0,001)
    constructyear=calculate_age(neighbors,average)
    yearsofconst.append(constructyear)
