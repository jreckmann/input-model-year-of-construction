data_s=open('/home/geophy/Desktop/buildings-year-known.csv','r')  #read in and prepare data file
data_slines=data_s.readlines()
data_list=[]
for i in range(1,len(data_slines)):
    data_slines[i]=data_slines[i].replace('\n','')
    data_list.append(data_slines[i].split(','))


data_subset_dense=[]
data_subset_normal=[]
data_subset_sparse=[]
for j in range(len(data_list)):
        if 51.8<float(data_list[j][1])<52.6 and float(data_list[j][2])<5.1: #high density area includes Rotterdam, Amsterdam & The Hague
            data_subset_dense.append(data_list[j])
        if 52.5<float(data_list[j][1])<53 and 5.5<float(data_list[j][2])<6: #low density area east of Amsterdam
            data_subset_sparse.append(data_list[j])
        if float(data_list[j][1])<51.6 and float(data_list[j][2])<4.5: #south-west part
            data_subset_normal.append(data_list[j])

a=open('/home/geophy/Desktop/buildings-subset-dense.csv','w')
b=open('/home/geophy/Desktop/buildings-subset-sparse.csv','w')
c=open('/home/geophy/Desktop/buildings-subset-normal.csv','w')

for k in range(len(data_subset_dense)):
    a.write('%s,%s,%s,%s\n'%(data_subset_dense[k][0],data_subset_dense[k][1],data_subset_dense[k][2],data_subset_dense[k][3]))

a.close()

for l in range(len(data_subset_sparse)):
    b.write('%s,%s,%s,%s\n'%(data_subset_sparse[l][0],data_subset_sparse[l][1],data_subset_sparse[l][2],data_subset_sparse[l][3]))

b.close()

for m in range(len(data_subset_normal)):
    c.write('%s,%s,%s,%s\n'%(data_subset_normal[m][0],data_subset_normal[m][1],data_subset_normal[m][2],data_subset_normal[m][3]))

c.close()