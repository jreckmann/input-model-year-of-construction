#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 20 15:06:29 2016

@author: geophy
"""

from keras.models import Sequential
from keras.layers import Dense
import numpy as np
import math
from datetime import datetime
from keras.optimizers import Adam
print(datetime.now().strftime('%H:%M:%S'))
from keras.layers import Dropout
from keras.constraints import maxnorm
from keras.regularizers import l2
from keras.layers import Flatten
from keras.layers.convolutional import Convolution2D
from keras.layers.convolutional import MaxPooling2D

np.random.seed(5)

def find_grid(data_point,Data_Matrix,Cell_Number):
    neighbor_grid=[]
    for q in range(int(round((data_point[1]*10000)-507281)-Cell_Number),int(round((data_point[1]*10000)-507281)+Cell_Number+1)):
        for p in range(int((round(data_point[2]*10000)-32713.15)-Cell_Number),int((round(data_point[2]*10000)-32713.15)+Cell_Number+1)):
            neighbor_grid.append(Data_Matrix[q,p])
    return neighbor_grid
    
CellNumber=10
#make matrix(175 points have the same rounded coordinates as others and are discarded)


from scipy import sparse
data_matrix=sparse.dok_matrix((33688+(2*CellNumber),53953+(2*CellNumber)),dtype=int)
data_list=np.genfromtxt('/home/geophy/Desktop/buildings-year-known.csv',delimiter=',')

for e in range(len(data_list)):
    if data_list[e][3]-900<=0:
        np.delete(data_list,e,0)
    else:
        data_list[e][3]=(data_list[e][3]-900)


counter=[]
for l in range(len(data_list)):
    if data_matrix[round(data_list[l,1]*10000-507281)+CellNumber,round((data_list[l,2]*10000)-32713.15)+CellNumber]!=0:
        data_matrix[round(data_list[l,1]*10000-507281)+CellNumber,round((data_list[l,2]*10000)-32713.15)+CellNumber]=round((data_matrix[round(data_list[l,1]*10000-507281)+CellNumber,round((data_list[l,2]*10000)-32713.15)+CellNumber]+(data_list[l,3]))/2.0)
    data_matrix[round(data_list[l,1]*10000-507281)+CellNumber,round((data_list[l,2]*10000)-32713.15)+CellNumber]=(data_list[l,3])

    

counter=0      
grid_set=[]
data_set=[]
for rpoint in np.random.randint(0,len(data_list),50000):
    grid_set.append(find_grid(data_list[rpoint],data_matrix,CellNumber))
    data_set.append(data_list[rpoint,3])



for q in range(len(data_set)):
    z=np.zeros(1120,int)
    z[int(data_set[q])]=1
    data_set[q]=z
       
train_grid=[]
train_data=[]
for u in range(5000):
    train_grid.append(grid_set[u])
    train_data.append(data_set[u])

train_grid,train_data=np.array(train_grid).reshape(-1,21,21,1),np.reshape(np.array(train_data),(-1,1120))
    
#test_data=[]
#test_grid=[]
#for s in range(5000,7500):
#    test_grid.append(grid_set[s])
#    test_data.append(data_set[s])

#test_grid,test_data=np.array(test_grid).reshape(-1,400),np.array(test_data).reshape(-1,1120)
    
eval_grid=[]
eval_data=[]
for z in range(45000,50000):
    eval_grid.append(grid_set[z])
    eval_data.append(data_set[z])

eval_grid,eval_data=np.array(eval_grid).reshape(-1,21,21,1),np.reshape(np.array(eval_data),(-1,1120))

print(datetime.now().strftime('%H:%M:%S'))
print("nn starts now")

X=train_grid
Y=train_data

model=Sequential()

model.add(Convolution2D(32,5,5,border_mode='valid',input_shape=(21,21,1),init='uniform',activation='relu',W_regularizer='l2'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.2))
model.add(Flatten())
model.add(Dense(400,activation='relu',W_regularizer='l2'))
model.add(Dense(1120,activation='softmax'))

ad=Adam(lr=0.001)

model.compile(loss='categorical_crossentropy',optimizer=ad,metrics=['accuracy'])

model.fit(X,Y,nb_epoch=200)

scores=model.evaluate(X,Y)
print(datetime.now().strftime('%H:%M:%S'))
print("Baseline Error: %.2f%%" % (100-scores[1]*100))

predictions=model.predict(eval_grid)

test_list=[]
for c in range(len(eval_grid)):
    for c2 in range(len(eval_data[c])):
        if eval_data[c][c2]!=0:
            test_list.append(c2)
            
diff=[]
pred=[]
for count in range(len(predictions)):
    pred.append(predictions[count].tolist().index(max(predictions[count]))+900)
    diff.append(math.fabs(predictions[count].tolist().index(max(predictions[count]))-test_list[count]))
 

summe=sum(diff)/float(len(diff))
print(datetime.now().strftime('%H:%M:%S'))
print('average difference: %f'%summe)