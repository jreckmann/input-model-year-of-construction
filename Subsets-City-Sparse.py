#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 10:06:17 2016

@author: geophy
"""
from sklearn.cluster import DBSCAN
import numpy as np

#read in datafile
data_list=np.genfromtxt('/home/geophy/Desktop/buildings-year-known.csv',delimiter=',')

#cluster the data with DBSCAN. Each central point of the cluster has 250buildings in a 1km radius around it.
#Values based on the building density of Geldrop, 150th biggest city in the Netherlands according to http://www.tageo.com/index-e-nl-cities-NL-step-2.htm.
db=DBSCAN(eps=1/6371.0088,min_samples=250,metric='haversine').fit(np.radians(data_list[:,[2,1]]))
core_samples_mask=np.zeros_like(db.labels_,dtype=bool)
core_samples_mask[db.core_sample_indices_]=True
labels=db.labels_
n_clusters_=len(set(labels))-(1 if -1 in labels else 0)
print('Estimated number of clusters: %d' % n_clusters_)



#divide data into points in a cluster and those that are not.
city=[]
sparse=[]
for q in range(len(labels)):  
    if labels[q]==-1:
        sparse.append(data_list[q])
    else:
        city.append(data_list[q])
 
#write into files
g=open('/home/geophy/Desktop/cities-subset.csv','w')
h=open('/home/geophy/Desktop/sparse-regions-subset.csv','w')

for p in range(len(city)):
    g.write('%i,%f,%f,%i\n'%(int(city[p][0]),city[p][1],city[p][2],int(city[p][3])))
g.close()

for r in range(len(sparse)):
    h.write('%i,%f,%f,%i\n'%(int(sparse[r][0]),sparse[r][1],sparse[r][2],int(sparse[r][3])))
h.close()