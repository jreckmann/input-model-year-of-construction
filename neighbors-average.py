#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 17 11:24:40 2016

@author: geophy
"""
import numpy as np
import random
import subprocess
import matplotlib.pyplot as plt
import math
#create empty lists for the boxplot
med=[]
uq=[]
lq=[]
uw=[]
lw=[]

file_input=raw_input('insert path to the datafile: ')
file_output=raw_input('insert path to outputfile: ')
file_image=raw_input('insert path to the image: ')
hist_output=raw_input('insert path to histogram: ')
image_title=raw_input('insert image title: ')
#neighborcount=int(raw_input('insert number of neighbors: '))
N=int(raw_input('insert number of testpoints: '))
runs=int(raw_input('insert number of runs: '))
bins=int(raw_input('insert the accuracy in years: '))

for m in [1,2,5,10]:#a run for every neighbor up to the inserted amount
    rights=[]#create an empty list to store the accuracy of the runs
    deltayears=[]
    for q in range(runs):
        full_data_list=np.genfromtxt('%s'%file_input,delimiter=',') #read data into numpy array
        rand=random.sample(range(len(full_data_list)),N)#get a list of random integers in the range of the data amount
        testpoints=np.zeros([N,4])#create an empty array
         
        for i in range(N):#fill the empty array with testpoints chosen with the list of random integers
            testpoints[i]=full_data_list[rand[i]]
    
        data_list=np.delete(full_data_list,[rand],0)#delete the testpoints from the data
        
        from sklearn.neighbors import NearestNeighbors#use the NeirestNeighbor algorithm from scikit learn to structure the data for further use
        neigh = NearestNeighbors(n_neighbors=m,metric='haversine')
        neigh.fit(np.radians(data_list[:,[1,2]]))
        
        year=[]
        for j in range(N):#for each of testpoint:
            years=0
            dummy,neighb=neigh.kneighbors(np.radians(testpoints[[j],[[1,2]]]))#find the neighbors of a testpoint
            for n in range(m):#add up the years of construction from the neighbors of the testpoint
                years+=float(data_list[neighb[0][n]][3])
            year.append(years/m)#get average year of construction around the testpoint
                
        right=0
        for l in range(N):
            deltayears.append(math.fabs(float(year[l])-float(testpoints[[l],[3]])))#check if the average year of construction around a point is within +-5 years of the year of construction of the testpoint
            if year[l]-bins<testpoints[[l],[3]]<year[l]+bins:
                right+=1
        rights.append(float(right)/float(N))
        #make a histogram of the accuracy
        plt.hist(deltayears,50,[1,300])
        plt.xlabel('|real year - average year|')
        plt.ylabel('Count')
        plt.title('%s'%image_title)
        plt.savefig('%s%i.png'%(hist_output,m),dpi=200)
        plt.clf()
    #calculate the quartiles for all runs having the same amount of neighbors
    med.append(np.percentile(rights,50))
    lq.append(np.percentile(rights,25))
    uq.append(np.percentile(rights,75))
    uw.append(np.percentile(rights,100))
    lw.append(np.percentile(rights,0))
    print(m)

g=open('%s'%(file_output),'w')#write the quartiles into a file
for k in range(4):
    g.write('%i %f %f %f %f %f\n'%([1,2,5,10][k],lw[k],lq[k],med[k],uq[k],uw[k]))
g.close()

proc=subprocess.Popen(['gnuplot','-p'],shell=True, stdin=subprocess.PIPE)#use gnuplot to make boxplots
proc.stdin.write('set boxwidth 0.5\n set style fill solid 0.1 border -1\n set xrange[0:%i]\n set xtics 1\n set yrange [0:1]\n set ytics 0.05\n'%(11))
proc.stdin.write('set xlabel \'Neighbors\' \n set ylabel \'Accuracy\'\n set title \'%s\'\n'%image_title)
proc.stdin.write('set terminal png size 1024,768\n')
proc.stdin.write('set output \'%s\'\n'%file_image)
proc.stdin.write('plot \'%s\' using 1:3:2:6:5 with candlesticks notitle lt -1 lw 2 whiskerbars, \'\' using 1:4:4:4:4 with candlesticks lt -1 lw 2 notitle\n'%file_output)
proc.stdin.write('quit\n')

